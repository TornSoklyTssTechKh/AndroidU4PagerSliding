package com.android4u.tornsokly.androidu4pagerslidingtabstrip;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentE extends Fragment {

    private static FragmentE instance=null;

    public static FragmentE getInstance() {
        if (instance == null) {
            instance = new FragmentE();
        }
        return instance;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_e, container, false);
    }

}
