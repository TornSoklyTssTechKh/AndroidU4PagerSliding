package com.android4u.tornsokly.androidu4pagerslidingtabstrip;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.astuetz.PagerSlidingTabStrip;

public class MainActivity extends AppCompatActivity implements ViewPager.OnPageChangeListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Initialize the ViewPager and set an adapter
        ViewPager pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(new TestAdapter(getSupportFragmentManager()));

       // Bind the tabs to the ViewPager
        PagerSlidingTabStrip tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);

        tabs.setViewPager(pager);
        tabs.setOnPageChangeListener(this);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        if (position == 0) {
           Log.i("Fragment","Home is selected");
        } else if (position == 1) {
            Log.i("Fragment","Videos is selected");
        } else if (position == 2) {
            Log.i("Fragment","Tutorial is selected");
        }else if(position == 3){
            Log.i("Fragment","Technology is selected");
        }else if(position == 4){
            Log.i("Fragment","News is selected");
        }else if(position == 5){
            Log.i("Fragment","About is selected");
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    public class TestAdapter extends FragmentPagerAdapter{

        public TestAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if (position == 0) {
                fragment = new FragmentA();
            } else if (position == 1) {
                fragment = new FragmentB();
            } else if (position == 2) {
                fragment = new FragmentC();
            }else if(position == 3){
                fragment = new FragmentD();
            }else if(position == 4){
                fragment = new FragmentE();
            }else if(position == 5){
                fragment = new FragmentF();
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 6;
        }
        @Override
        public CharSequence getPageTitle(int position) {
            if(position == 0){
                return "Home";
            }else if(position==1){
                return "Videos";
            }else if(position==2){
                return "Tutorial";
            }else if(position==3){
                return "Technology";
            }else if(position==4){
                return "News";
            }else if(position==5){
                return "About";
            }
            return super.getPageTitle(position);
        }
    }
}
