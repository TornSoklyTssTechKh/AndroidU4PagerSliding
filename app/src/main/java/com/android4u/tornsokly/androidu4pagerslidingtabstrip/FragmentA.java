package com.android4u.tornsokly.androidu4pagerslidingtabstrip;


import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentA extends Fragment {

    private static FragmentA instance=null;

    public static FragmentA getInstance() {
        if (instance == null) {
            instance = new FragmentA();
        }
        return instance;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i("FragmentA","onCreateView()");
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_a, container, false);
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.i("FragmentA","onStart()");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.i("FragmentA","onResume()");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i("FragmentA","onSaveInstanceState()");
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        Log.i("FragmentA","onViewStateRestored()");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i("FragmentA","onDestroy()");
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i("FragmentA","onDestroyView()");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.i("FragmentA","onAttach()");
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.i("FragmentA","onActivityCreated()");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        Log.i("FragmentA","onDetach()");
    }
}
